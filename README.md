# Escape Room Collaboration

This repository holds the Unity files, CMS link, as well as documentations of the Escape Room game.

## Unity files
The Unity files of the Escape room game can be found [here](https://gitlab.com/ABELiusSimar/escape-room-roc-tilburg/-/tree/main/Escape%20Room%20(Concept%20A)). The files contains all of the [assets](https://gitlab.com/ABELiusSimar/escape-room-roc-tilburg/-/tree/main/Escape%20Room%20(Concept%20A)/Assets) and [scripts](https://gitlab.com/ABELiusSimar/escape-room-roc-tilburg/-/tree/main/Escape%20Room%20(Concept%20A)/Assets/Scripts) of the game made using Unity version 2020.3.30f1.
(Updated)
The first version of the Multiplayer Test (Also made with 2020.3.30f1) is now uploaded [here](https://gitlab.com/ABELiusSimar/escape-room-roc-tilburg/-/tree/main/Multiplayer%20Test).

## CMS
The CMS is uploaded to another repository, found [here](https://github.com/gustass5/EscapeRoomManagement).

## Documentations
This repository contains all the list of design documents (and visual designs) of every iteration that were made for the Escape Room game (Not final, some more documents and visual design may be uploaded).

- Project Plan:
    - [Project Plan](https://gitlab.com/ABELiusSimar/escape-room-roc-tilburg/-/tree/main/Documentations/Project%20Plan) for the entire phase of the project.
- Game Design document:
    - [Version 1](https://gitlab.com/ABELiusSimar/escape-room-roc-tilburg/-/tree/main/Documentations/Game%20Design%20Documents), with initial MDA (Mechanics, Dynamics, and Aesthetics) and no pictures of the Theme and Mood.
    - [Version 2](https://gitlab.com/ABELiusSimar/escape-room-roc-tilburg/-/tree/main/Documentations/Game%20Design%20Documents), Finalised MDA as well as some design pictures in Theme and Mood.
- Wireframes:
    - [Version 1](https://gitlab.com/ABELiusSimar/escape-room-roc-tilburg/-/tree/main/Documentations/Wireframes/V1), Design made to be used for handheld device (Tablets and Phones).
    - [Version 2](https://gitlab.com/ABELiusSimar/escape-room-roc-tilburg/-/tree/main/Documentations/Wireframes/V2), Design was changed to fit desktop devices.
- MoSCoW list:
    - [Document](https://gitlab.com/ABELiusSimar/escape-room-roc-tilburg/-/tree/main/Documentations/Moscow%20List) showing the lists of all the requirements of the project.
- Room Design:
    - [Shows](https://gitlab.com/ABELiusSimar/escape-room-roc-tilburg/-/tree/main/Documentations/Rooms) the room designs used for the Project.
- Branding Guide:
    - [Document](https://gitlab.com/ABELiusSimar/escape-room-roc-tilburg/-/tree/main/Documentations/Branding%20Guide) that shows the style of the Escape Room Game.
- Transfer Documents:
    - This contains all the necessary research [documents](https://gitlab.com/ABELiusSimar/escape-room-roc-tilburg/-/tree/main/Documentations/Transfer%20Folder/Transfer%20Folder) of this project.
